import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-root',
  template: `
  <p>{{msg}}</p>
  <router-outlet> </router-outlet>
  `,
  styles: [``]
})
export class AppComponent implements OnInit {

  title = 'notebook-app';
  public msg;

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.getBackendMsg();
  }

  getBackendMsg() {
    const url = "http://localhost:8080/api/TODOAPP/";
    const headers = new HttpHeaders().set('Content-Type', 'text/plain; charset=utf-8');
    this.http.get(url, { headers, responseType: 'text' as 'json' }).subscribe(res => this.msg = res);
  }

}