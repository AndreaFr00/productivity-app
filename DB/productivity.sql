create database if not exists productivityDB;
use productivityDB;

create table if not exists todo (
    todoID BIGINT AUTO_INCREMENT PRIMARY KEY,
    description text NOT NULL,
    createDate datetime NOT NULL,
    completed boolean NOT NULL,
    completedDate datetime
);
