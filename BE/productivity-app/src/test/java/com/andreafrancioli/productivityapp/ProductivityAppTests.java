package com.andreafrancioli.productivityapp;

import com.andreafrancioli.productivityapp.Model.Todo;
import com.andreafrancioli.productivityapp.Repository.TodoRepository;
import com.andreafrancioli.productivityapp.Service.TodoService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
class ProductivityAppTests {

	@Autowired
	private TodoService todoService;

	@MockBean
	private TodoRepository todoRepository;

	private Todo todo;

	@BeforeEach
	public void init() {
		todo = new Todo();
		todo.setTodoID(333);
		todo.setDescription("todoTest1");
		todo.setCreateDate(new Date());
		todo.setCompleted(true);
		todo.setCompletedDate(new Date());
	}

	@Test
	public void getAllTodoTest() {
		List<Todo> allTodo = new ArrayList<Todo>();
		allTodo.add(todo);

		when(todoRepository.findAll()).thenReturn(allTodo);

		assertEquals(1, todoService.getAllTodo().size());
		verify(todoRepository, times(1)).findAll();
		verifyNoMoreInteractions(todoRepository);
	}

	@Test
	public void addTodoTest() {
		when(todoRepository.save(todo)).thenReturn(todo);

		assertEquals(todo, todoService.addTodo(todo));
		verify(todoRepository, times(1)).save(todo);
		verifyNoMoreInteractions(todoRepository);
	}

	@Test
	public void deleteTodoTest() {
		todoService.deleteTodo(todo.getTodoID());

		verify(todoRepository, times(1)).deleteById(todo.getTodoID());
		verifyNoMoreInteractions(todoRepository);
	}

}
