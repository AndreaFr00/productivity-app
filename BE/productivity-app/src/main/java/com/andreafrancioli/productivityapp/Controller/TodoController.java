package com.andreafrancioli.productivityapp.Controller;

import java.util.List;

import com.andreafrancioli.productivityapp.Model.Todo;
import com.andreafrancioli.productivityapp.Service.TodoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/TODO/")
public class TodoController {

    @Autowired
    private TodoService todoService;

    @GetMapping()
    public String test() {
        return "Hello World (spring)";
    }

    @GetMapping("getTodo/{id}")
    public ResponseEntity<Todo> getTodoById(@PathVariable long id) {
        return new ResponseEntity<>(todoService.getTodoById(id), HttpStatus.OK);
    }

    @GetMapping("getAllTodo")
    public ResponseEntity<List<Todo>> getAllTodo() {
        return new ResponseEntity<>(todoService.getAllTodo(), HttpStatus.OK);
    }

    @PostMapping("addTodo")
    public ResponseEntity<Todo> addTodo(@RequestBody Todo todo) {
        return new ResponseEntity<>(todoService.addTodo(todo), HttpStatus.OK);
    }

    @PutMapping("updateTodo")
    public ResponseEntity<Todo> updateTodo(@RequestBody Todo todo) {
        return new ResponseEntity<>(todoService.updateTodo(todo), HttpStatus.OK);
    }

    @DeleteMapping("deleteTodo/{id}")
    public void deleteTodo(@PathVariable long id) {
        todoService.deleteTodo(id);
    }

}
