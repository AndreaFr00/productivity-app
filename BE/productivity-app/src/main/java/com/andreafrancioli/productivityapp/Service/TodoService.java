package com.andreafrancioli.productivityapp.Service;

import java.util.List;

import com.andreafrancioli.productivityapp.Model.Todo;
import com.andreafrancioli.productivityapp.Repository.TodoRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TodoService {

    @Autowired
    private TodoRepository todoRepository;

    public List<Todo> getAllTodo() {
        return todoRepository.findAll();
    }

    public Todo getTodoById(long id) {
        return todoRepository.findById(id).orElseThrow();
    }

    public Todo addTodo(Todo todo) {
        return todoRepository.save(todo);
    }

    public Todo updateTodo(Todo todo) {
        Todo existingTodo = todoRepository.findById(todo.getTodoID()).orElseThrow();

        existingTodo.setDescription(todo.getDescription());
        existingTodo.setCompleted(todo.getCompleted());
        existingTodo.setCompletedDate(todo.getCompletedDate());

        return todoRepository.save(existingTodo);
    }

    public void deleteTodo(long id) {
        todoRepository.deleteById(id);
    }

}
