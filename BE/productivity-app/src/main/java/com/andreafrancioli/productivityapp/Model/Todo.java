package com.andreafrancioli.productivityapp.Model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "todo", schema = "productivityDB")
public class Todo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "todoID")
    private long todoID;

    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "createdate", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;

    @Column(name = "completed", nullable = false)
    private boolean completed;

    @Column(name = "completeddate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date completedDate;

    public long getTodoID() {
        return this.todoID;
    }

    public String getDescription() {
        return this.description;
    }

    public Date getCreateDate() {
        return this.createDate;
    }

    public boolean getCompleted() {
        return this.completed;
    }

    public Date getCompletedDate() {
        return this.completedDate;
    }

    public void setTodoID(long todoID) {
        this.todoID = todoID;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public void setCompletedDate(Date completedDate) {
        this.completedDate = completedDate;
    }

}
