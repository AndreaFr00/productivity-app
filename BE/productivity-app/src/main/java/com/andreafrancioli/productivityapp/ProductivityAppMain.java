package com.andreafrancioli.productivityapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProductivityAppMain {

	public static void main(String[] args) {
		SpringApplication.run(ProductivityAppMain.class, args);
	}

}
