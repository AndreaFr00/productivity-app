package com.andreafrancioli.productivityapp.Repository;

import com.andreafrancioli.productivityapp.Model.Todo;

import org.springframework.data.jpa.repository.JpaRepository;

public interface TodoRepository extends JpaRepository<Todo, Long> {

}
